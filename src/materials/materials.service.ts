import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateMaterialDto } from './dto/create-material.dto';
import { UpdateMaterialDto } from './dto/update-material.dto';
import { Material } from './entities/material.entity';

@Injectable()
export class MaterialsService {
  lastId: number = 1;
  materials: Material[] = [
    { id: 1, name: 'ผงชาเขียว', Amount: 1, unit: 'กิโลกรัม', price: 50 },
  ];

  create(createMaterialDto: CreateMaterialDto) {
    this.lastId++;
    const newMaterial = { ...createMaterialDto, id: this.lastId };
    this.materials.push(newMaterial);
    return newMaterial;
  }

  findAll() {
    return this.materials;
  }

  findOne(id: number) {
    const index = this.materials.findIndex((material) => material.id === id);
    if (index < 0) {
      throw new NotFoundException();
    }
    return this.materials[index];
  }

  update(id: number, updateMaterialDto: UpdateMaterialDto) {
    const index = this.materials.findIndex((material) => material.id === id);
    if (index < 0) {
      throw new NotFoundException();
    }
    this.materials[index] = { ...this.materials[index], ...updateMaterialDto };
    return this.materials[index];
  }

  remove(id: number) {
    const index = this.materials.findIndex((material) => material.id === id);
    if (index < 0) {
      throw new NotFoundException();
    }
    const delMaterial = this.materials[index];
    this.materials.splice(index, 1);
    return delMaterial;
  }
}
